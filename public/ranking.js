
getHistory()

let experienceRank = []
let playerInformationArray = [] //all details
let playerTotalGameRank = []
let winRateRank = []
let playerHighestScoreRank = []

async function getHistory() {
    const res = await fetch("/all-history")
    const data = await res.json()

    console.log(data)
    if (!data) {
        console.log("No  history")
        return
    }

    let history = data.history
    const username = data.user
    
    

    experienceRank = JSON.parse(JSON.stringify(username.sort((a, b) => b.experience - a.experience)))

    console.log(history)
    username.forEach(item => {
        let playerInformation = { name: item.name, totalGame: 0, score: 0, winGame: 0 }
        
        let prevUUID = ""
        history.forEach(historyData => {
            if(!(prevUUID == historyData.uuid)){
                let gameRank = (historyData.result).findIndex(data => data.name == item.name)
                if (gameRank == -1) return
                playerInformation["score"] += (historyData.result)[gameRank].score
                playerInformation["totalGame"] += 1
                if (gameRank == 0) {
                    playerInformation["winGame"] += 1
                }
                prevUUID=historyData.uuid //for next checking
            }
        })
        playerInformationArray.push(playerInformation)
    })

    playerTotalGameRank = JSON.parse(JSON.stringify(playerInformationArray.sort((a, b) => b.totalGame - a.totalGame)))
    winRateRank = JSON.parse(JSON.stringify(playerInformationArray.sort((a, b) => b.winGame / b.totalGame - a.winGame / a.totalGame)))
    playerHighestScoreRank = JSON.parse(JSON.stringify(playerInformationArray.sort((a, b) => b.score - a.score)))
}

let rankTable = document.querySelector(".ranking-table")

document.querySelector(".experience-rank").addEventListener("click", () => {
    rankTable.innerHTML = ""
    let row = document.createElement("div")
    row.classList.add("rankingRow")
    row.innerHTML += `
    <div class="rank-item">Rank</div>
    <div class="rank-item">Name</div>
    <div class="rank-item">Experience</div>
    `
    rankTable.appendChild(row)

    experienceRank.forEach((item, index) => {
        let classAdd;
        if (index == 0) {
            classAdd = "rank1"
        } else if (index == 1) {
            classAdd = "rank2"
        } else if (index == 2) {
            classAdd = "rank3"
        } else {
            classAdd = ""
        }
        let row = document.createElement("div")
        row.classList.add("rankingRow")
        row.innerHTML += `
            <div class="rank-item ${classAdd}">${index + 1}</div>
            <div class="rank-item ${classAdd}">${item.name}</div>
            <div class="rank-item ${classAdd}">${item.experience}</div>
            `
        rankTable.appendChild(row)
    })
})



document.querySelector(".total-game-rank").addEventListener("click", () => {
    rankTable.innerHTML = ""
    let row = document.createElement("div")
    row.classList.add("rankingRow")
    row.innerHTML += `
    <div class="rank-item">Rank</div>
    <div class="rank-item">Name</div>
    <div class="rank-item">Total Game</div>
    `
    rankTable.appendChild(row)

    playerTotalGameRank.forEach((item, index) => {
        let classAdd;
        if (index == 0) {
            classAdd = "rank1"
        } else if (index == 1) {
            classAdd = "rank2"
        } else if (index == 2) {
            classAdd = "rank3"
        } else {
            classAdd = ""
        }
        let row = document.createElement("div")
        row.classList.add("rankingRow")
        row.innerHTML += `
            <div class="rank-item ${classAdd}">${index + 1}</div>
            <div class="rank-item ${classAdd}">${item.name}</div>
            <div class="rank-item ${classAdd}">${item.totalGame}</div>
            `
        rankTable.appendChild(row)
    })
})



document.querySelector(".score-rank").addEventListener("click", () => {
    rankTable.innerHTML = ""
    let row = document.createElement("div")
    row.classList.add("rankingRow")
    row.innerHTML += `
    <div class="rank-item">Rank</div>
    <div class="rank-item">Name</div>
    <div class="rank-item">Score</div>
    `
    rankTable.appendChild(row)

    playerHighestScoreRank.forEach((item, index) => {
        let classAdd;
        if (index == 0) {
            classAdd = "rank1"
        } else if (index == 1) {
            classAdd = "rank2"
        } else if (index == 2) {
            classAdd = "rank3"
        } else {
            classAdd = ""
        }
        let row = document.createElement("div")
        row.classList.add("rankingRow")
        row.innerHTML += `
            <div class="rank-item ${classAdd}">${index + 1}</div>
            <div class="rank-item ${classAdd}">${item.name}</div>
            <div class="rank-item ${classAdd}">${item.score}</div>
            `
        rankTable.appendChild(row)
    })
})


document.querySelector(".winrate-rank").addEventListener("click", () => {
    rankTable.innerHTML = ""
    let row = document.createElement("div")
    row.classList.add("rankingRow")
    row.innerHTML += `
    <div class="rank-item">Rank</div>
    <div class="rank-item">Name</div>
    <div class="rank-item">Win Rate</div>
    `
    rankTable.appendChild(row)

    winRateRank.forEach((item, index) => {
        if(item.totalGame==0)return
        let classAdd;
        if (index == 0) {
            classAdd = "rank1"
        } else if (index == 1) {
            classAdd = "rank2"
        } else if (index == 2) {
            classAdd = "rank3"
        } else {
            classAdd = ""
        }
        let row = document.createElement("div")
        row.classList.add("rankingRow")
        row.innerHTML += `
            <div class="rank-item ${classAdd}">${index + 1}</div>
            <div class="rank-item ${classAdd}">${item.name}</div>
            <div class="rank-item ${classAdd}">${(item.winGame*100/item.totalGame).toFixed(0)}%</div>
            `
        rankTable.appendChild(row)
    })
})