import {Client} from 'pg';

export class RoomService{
	constructor(private client:Client){}

	async getUserId(username:string){
		return (await this.client.query(/*sql*/`select id from users where users.name = $1`,[username])).rows[0].id
	}

	async userResult(userId:number){
		return (await this.client.query(/*sql*/`select is_in_room,is_ready,is_playing from users
		where users.id = $1`,[userId]))
		.rows[0]
	}

	async insertRoom(name:string,uuId:string,username:string){
		await this.client.query(/*sql*/`insert into rooms(name,uuId) values ($1,$2)`,[name,uuId]);
		await this.client.query(/*sql*/`update users set is_host = $1 where name = $2`,[true,username])
	}

	async doesRoomExist(uuId:string){
		return (await this.client.query(/*sql*/`select * from rooms where uuid = $1`,[uuId])).rows[0]
	}

	async roomIdResult(uuId:string){
		return (await this.client.query(/*sql*/`select id,num_of_players,is_started,is_ended from rooms
		where uuid = $1`,[uuId])).rows[0]
	}

	async inWhichRoom(userId:number){
		return (await this.client.query(/*sql*/`select room_id from user_room_mapping where user_id = $1`,[userId]))
		.rows[0].room_id;
	}

	async updateRoomCondition(userId:number,roomId:number,username:string,uuId:string){
		await this.client.query(/*sql*/`insert into user_room_mapping(user_id,room_id) values ($1,$2)`,[userId,roomId]);
		await this.client.query(/*sql*/`update users set is_in_room = $1 where users.name = $2`,[true,username]);
		// console.log("a")
		await this.client.query(/*sql*/`update rooms set num_of_players = num_of_players + 1 where uuid = $1`,[uuId]);
	}

	async changeHost(uuId:string){
		return (await this.client.query(/*sql*/ `select users.name from rooms left outer join user_room_mapping
		on rooms.id = user_room_mapping.room_id left outer join users on users.id = user_room_mapping.user_id
		where is_host = $1 and uuid = $2`,[true,uuId])).rows[0].name;
	}

	async getPlayerOrder(username:string){
		return (await this.client.query(/*sql*/`select player_order from users where users.name = $1`,
		[username])).rows[0].player_order;
	}

	async whichPlayer(uuId:string){
		return (await this.client.query(/*sql*/`select which_player from rooms where uuid = $1`,[uuId])).rows[0].which_player;
	}

	async updateQuitPlayer(playerOrder:number,uuId:string){
		await this.client.query(/*sql*/`update rooms set quit_player = array_append(quit_player, $1)
		where uuid = $2`,[playerOrder,uuId]);
	}

	async getCurrentTurn(uuId:string){
		return (await this.client.query(/*sql*/`select current_turn from rooms where uuid = $1`,[uuId])).rows[0].current_turn;
	}

	async getNumberOfQuitPlayer(uuId:string){
		return (await this.client.query(/*sql*/`select quit_player from rooms where uuid = $1`,[uuId])).rows[0].quit_player;
	}

	async updateNumberOfPlayers(uuId:string){
		await this.client.query(/*sql*/`update rooms set num_of_players = num_of_players - 1 where uuid = $1`,[uuId]);
		await this.client.query(/*sql*/`update rooms set num_of_ready_players = num_of_ready_players - 1 where uuid = $1`,
		[uuId]);
	}

	async deleteDuplicateUser(userId:number){
		await this.client.query(/*sql*/`delete from user_room_mapping where user_id = $1`, [userId])
	}
}