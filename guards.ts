import express,{Request,Response,NextFunction} from 'express';

export function isLoggedInAPI(req:Request,res:Response,next:NextFunction){
    if(req.session['user']){
        next();
    }else{
        res.json({message:"Unauthorized"});
    }
}

export function isLoggedIn(req:Request,res:Response,next:express.NextFunction){
    if(req.session['user']){
        next();
    }else{
        res.redirect("/");
    }
}