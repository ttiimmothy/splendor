import {playerOrder,socket,playerArray,numberOfPlayer} from "./playroom.js"
import {messageBox} from "./roomButton.js"
let agreeSavePlayer = 0
let reciever = 0
export const saveGameButton = document.querySelector(".save-game-button")
saveGameButton.addEventListener("click", saveGame)
function saveGame(){
    messageBox("Save Game And Leave Room ?",(choice)=>{
        if(choice){
            if(numberOfPlayer == 1){
                socket.emit("all-player-agree-save");
            }else{
                socket.emit("save-game-request",{"username":playerArray[playerOrder].name,"playerOrder":playerOrder});
                document.querySelector("#message-box").style.display = "none";
                document.querySelector("#message-box").parentNode.style.zIndex = "0";
            }
        }else{
            document.querySelector("#message-box").style.display = "none";
            document.querySelector("#message-box").parentNode.style.zIndex = "0";
        }
    })
}
socket.on("save-request",(username)=>{
    messageBox(`User ${username} ask to Save Game And Leave Room , Agree?`,(choice)=>{
        if(choice){
            socket.emit("save-game-recieve",{"username":username,"result":true});
            document.querySelector("#message-box").style.display = "none";
            document.querySelector("#message-box").parentNode.style.zIndex = "0";
        }else{
            socket.emit("save-game-recieve",{"username":username,"result":false});
            document.querySelector("#message-box").style.display = "none";
            document.querySelector("#message-box").parentNode.style.zIndex = "0";
        }
    })
})
socket.on("player-response",async(playerResponse)=>{
    reciever += 1;
    if(playerResponse){
        agreeSavePlayer += 1;
        document.querySelector(".save-game-agree-player").innerText += "+";
    }else{
        document.querySelector(".save-game-agree-player").innerText += "/";
    }

    if(reciever === numberOfPlayer - 1){
        document.querySelector(".save-game-agree-player").innerText = "";
        if(agreeSavePlayer == numberOfPlayer - 1){
            socket.emit("all-player-agree-save", "");
        }
        agreeSavePlayer = 0;
        reciever = 0;
    }
})

socket.on("save-game-ready",()=>{
    window.location.href = "/lobby.html";
})