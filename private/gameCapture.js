import { socket } from "./playroom.js";

export let recorder;

export const captureStartButton = document.querySelector(".capture-button")
const stopCaptureButton = document.querySelector(".stop-capture-button")

captureStartButton.addEventListener("click",startRecording)
stopCaptureButton.addEventListener("click",stopRecorder)

export function startRecorder() {
    recorder.start();


}

export function stopRecorder() {
    recorder.stop();
}

export async function startRecording() {
    const stream = await navigator.mediaDevices.getDisplayMedia({
        video: { mediaSource: "screen" }
    });
    recorder = new MediaRecorder(stream);

    startRecorder()
    console.log(recorder.state);
    if(recorder.state=="recording"){
        captureStartButton.innerText = `${recorder.state}`
    }
    const chunks = [];
    recorder.ondataavailable = ev => chunks.push(ev.data);
    recorder.onstop = async () => {

        socket.emit("gameRecord",chunks)
        socket.on("gameRecordSend",videoBlob=>{
                    const blob = new Blob(videoBlob, { type: videoBlob[0].type });
            const gameReplay = document.querySelector(".replay")
            gameReplay.style.width="300px"
            gameReplay.style.height="300px"
            gameReplay.src = URL.createObjectURL(blob);
            stream.getVideoTracks()[0].stop();
        })
    };
}

