import {gameTable} from "./table.js"
import {currentPlayer,dataTransfer,endTurn,socket} from "./playroom.js"
import {playerArray} from "./playroom.js"
import {getSound} from "./sound.js"

const tableShownCard = document.querySelectorAll(".shown-card")
const buyCardConfirmBox = document.querySelector("#buy-card-confirm")
const selectedCard = document.querySelector("#selected-card")

export let selectedCardForBuy;
export function modifyselectedCardForBuy(value){selectedCardForBuy=value}
export let buyCardPosition;
export function modifybuyCardPosition(value){buyCardPosition=value}
let isEnoughToken = false;
let clickCardType;
export function buyShownDeckCardOn() {
    tableShownCard.forEach((item, index) => {
        item.addEventListener("click", async() => {
            await getSound("click") //click card sound effect
            item.classList.remove("animate__animated")
            item.classList.remove("animate__slideInLeft")
            buyCardPosition = "table"

            let clickCardData = item.getAttribute("data-id")
            clickCardType = { //-1 mean it is from deck ,  0 -4 mean it is from shown deck
                "tier": clickCardData.split("-")[0],
                "shownCardArrayIndex": Number(clickCardData.split("-")[1]) - 1
            }
            switch (clickCardType.tier) {
                case "LL":
                        selectedCardForBuy = gameTable.lowLevelShownDeck[clickCardType.shownCardArrayIndex]
                    break
                case "ML":
                        selectedCardForBuy = gameTable.midLevelShownDeck[clickCardType.shownCardArrayIndex]
                    break
                case "HL":
                        selectedCardForBuy = gameTable.highLevelShownDeck[clickCardType.shownCardArrayIndex]
                    break
            }

            buyCardBoxCreate()
            //choose in hold card deck
        })
    })
}


function checkFreeBuyCard(card) {
    for (let key in card.cost) {
        const tokenLack = card.cost[key] - playerArray[currentPlayer].totalProduction()[key]
        if (tokenLack > 0) {
            return false
        }
    }
    return true
}


export function buyCardBoxCreate() {
    buyCardConfirmBox.style.display = "flex"

    //Add Buy box tag
    let costItemDiv = "";
    for (let key in selectedCardForBuy.cost) {
        if (selectedCardForBuy.cost[key] != 0) {
            costItemDiv += `
                    <div class="${key} cost-circle">
                        ${selectedCardForBuy.cost[key]}
                    </div>`
        }
    }
    selectedCard.innerHTML = `
            <div class="${selectedCardForBuy.production} big-card-cover ">
                <div class="card-top">
                    <div class="score">${selectedCardForBuy.score}</div>
                    <div class="production ${selectedCardForBuy.production}"></div>
                </div>
                <div class="card-bottom">
                    <div class="cost">
                        ${costItemDiv}
                    </div>
                    <div>
                        ${selectedCardForBuy.id}
                    </div>
                </div>
            </div>
            `


    document.querySelector("#buy-card-production-container").innerHTML = `
        <div class="buy-card-production">
        <img src="/image/ruby2.png" class="img-fluid" alt="ruby">
        ${playerArray[currentPlayer].totalProduction()["red"]}</div>

        <div class="buy-card-production">
        <img src="/image/jade.png" class="img-fluid jade-image" alt="jade">
        ${playerArray[currentPlayer].totalProduction()["green"]}</div>

        <div class="buy-card-production">
        <img src="/image/topaz2.png" class="img-fluid " alt="topaz">
        ${playerArray[currentPlayer].totalProduction()["blue"]}</div>

        <div class="buy-card-production">
        <img src="/image/obsidian2.png" class="img-fluid" alt="obsidian">
        ${playerArray[currentPlayer].totalProduction()["brown"]}</div>

        <div class="buy-card-production">
        <img src="/image/diamond.png" class="img-fluid" alt="diamond">
        ${playerArray[currentPlayer].totalProduction()["white"]}</div>`

    //check is it enough already
    if (checkFreeBuyCard(selectedCardForBuy)) {
        buyCardConfirm.disabled = false
        isEnoughToken = true
    }
}



let buyCardToken = document.querySelectorAll(".buy-card-token div")
let virtualTokenForBuyCard = { red: 0, green: 0, blue: 0, brown: 0, white: 0, gold: 0 }

//cancel box button
const cancelButton = document.querySelector("#cancel-button")
cancelButton.addEventListener("click", cancelBox)
function cancelBox() {
    resetBuyCardToken()
    buyCardConfirmBox.style.display = "none"
}
//reset token button
const buyCardTokenResetButton = document.querySelector("#reset-button")
buyCardTokenResetButton.addEventListener("click", resetBuyCardToken)
function resetBuyCardToken() {
    buyCardConfirm.disabled = true
    isEnoughToken = false
    buyCardToken.forEach((item, index) => {
        virtualTokenForBuyCard[Object.keys(virtualTokenForBuyCard)[index]] = 0
        item.innerText = virtualTokenForBuyCard[Object.keys(virtualTokenForBuyCard)[index]]
    })
}
//confirm buy
const buyCardConfirm = document.querySelector("#confirm-button")
buyCardConfirm.addEventListener("click", buyCardProcess)
async function buyCardProcess() {

    let detailTag = document.querySelector(`#player${currentPlayer+1} .detail`)
    detailTag.classList.remove("animate__animated")
    detailTag.classList.remove("animate__rubberBand")
    playerArray[currentPlayer].buyCard(selectedCardForBuy, buyCardPosition, virtualTokenForBuyCard)
    cancelBox()
    await dataTransfer()
    socket.emit("buy-card",{player:currentPlayer,cardBuyType:clickCardType,
        buyFrom:buyCardPosition,virtualPay:virtualTokenForBuyCard})
    endTurn()
}

//set rule for buy card token
buyCardToken.forEach((item, index) => {
    const tokenColorKey = Object.keys(virtualTokenForBuyCard)[index]
    item.innerText = virtualTokenForBuyCard[tokenColorKey]
    item.addEventListener("click", e => {

        if (isEnoughToken) {
            console.log("is enough")
            return
        }
        if (selectedCardForBuy.cost[tokenColorKey] == undefined && tokenColorKey != "gold") {
            console.log("this token is not required")
            return //if the token is not required
        }
        if (selectedCardForBuy.cost[tokenColorKey] <= playerArray[currentPlayer].totalProduction()[tokenColorKey] + virtualTokenForBuyCard[tokenColorKey]) {
            console.log("you can't pay more  than the card cost")
            return // cant pay more than card cost
        }
        if (virtualTokenForBuyCard[tokenColorKey] == playerArray[currentPlayer].resource[tokenColorKey]) {
            console.log("not enough token")
            return //if player not enough token
        }
        virtualTokenForBuyCard[tokenColorKey] += 1
        e.currentTarget.innerText = virtualTokenForBuyCard[tokenColorKey]
        let virtualGold = virtualTokenForBuyCard["gold"]
        for (let key in selectedCardForBuy.cost) {
            const tokenLack = selectedCardForBuy.cost[key] - playerArray[currentPlayer].totalProduction()[key] - virtualTokenForBuyCard[key]
            console.log(tokenLack)
            if (tokenLack <= 0) {
                continue
            } else {
                virtualGold -= tokenLack
            }
        }
        console.log("virtualGold : ", virtualGold)
        if (virtualGold >= 0) {
            isEnoughToken = true
            buyCardConfirm.disabled = false
            return
        }
    })
})