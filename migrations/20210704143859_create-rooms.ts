import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("rooms",(table) => {
		table.increments();
		table.string("name");
		table.string("uuid");
		table.boolean("is_started").defaultTo(false);
		table.boolean("is_ended").defaultTo(false);
		table.integer("num_of_players").defaultTo(0);
		table.integer("num_of_ready_players").defaultTo(0);
		table.integer("which_player").defaultTo(0);
		table.specificType("quit_player","integer[]").defaultTo("{}");
		table.integer("current_turn").defaultTo(0);
		table.text("transmission");
		table.specificType("playerarray","text[]");
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("rooms");
}