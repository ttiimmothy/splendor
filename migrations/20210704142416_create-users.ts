import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("users",(table) => {
		table.increments();
		table.text("name");
		table.string("password");
		table.boolean("is_in_room").defaultTo(false);
		table.boolean("is_ready").defaultTo(false);
		table.boolean("is_playing").defaultTo(false);
		table.integer("player_order").defaultTo(0);
		table.text("email");
		table.string("icon");
		table.date("birthday");
		table.string("degree");
		table.boolean("is_host").defaultTo(false);
		table.integer("experience").defaultTo(0);
		table.string("skype").defaultTo("");
		table.string("bio").defaultTo("");
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("users");
}