// import {SoundService} from "../services/SoundService";s
import {Request,Response} from "express";
import path from "path";
import {logger} from "../logger";

export class SoundController{
	constructor(){}

	sound = async(req:Request,res:Response) => {
		try{
			res.sendFile(path.resolve(`./soundtrack/${req.query.name}.mp3`));
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	soundLobby = async(req:Request,res:Response) => {
		try{
			res.sendFile(path.resolve(`./soundtrack/${req.query.name}.mp3`));
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}
}