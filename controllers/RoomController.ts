import {Request,Response} from "express";
import {allRoomData,endTurn,io,playerReset,reConnection,uuidGenerator} from "../main";
import {RoomService} from "../services/RoomService";
import path from "path";
import {logger} from "../logger";

export class RoomController{
	constructor(private roomService:RoomService){}

	createRoom = async(req:Request,res:Response) => {
		try{
			const name = "room1";
			const username = req.session["user"].name;
			const userId = await this.roomService.getUserId(username);
			const userResult = await this.roomService.userResult(userId);
			if(userResult.is_in_room || userResult.is_playing || userResult.is_ready){
				res.json({success:false});
			}else{
				const uuId = await uuidGenerator()
				await this.roomService.insertRoom(name,uuId,username)
				req.session["uuId"] = uuId;
				res.json(uuId);
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	joinRoom = async(req:Request,res:Response) => {
		try{
			const username = req.session["user"].name;
			const uuId = req.params.uuId;
			// console.log(uuId)
			const doesRoomExist = await this.roomService.doesRoomExist(uuId);
			if (doesRoomExist){
				const roomIdResult = await this.roomService.roomIdResult(uuId)
				const userId = await this.roomService.getUserId(username);
				const userResult = await this.roomService.userResult(userId)
				// console.log(roomIdResult);
				// console.log(userResult)
				// console.log(userId)

				if(userResult.is_playing){
					const inWhichRoom = await this.roomService.inWhichRoom(userId)
					// console.log(inWhichRoom)
					if(roomIdResult.id === inWhichRoom){
						// console.log(uuId)
						req.session["uuId"] = uuId;
						res.sendFile(path.resolve("./private/playroom.html"));
							if(roomIdResult.is_started){
								reConnection(username,uuId)
							}
					}else{
						res.json({message:"You do not belong to this room"});
					}
				}else if(!roomIdResult.is_started && !userResult.is_in_room && (roomIdResult.num_of_players < 4)
				&& !roomIdResult.is_ended){
					await this.roomService.updateRoomCondition(userId,roomIdResult.id,username,uuId)
					req.session["uuId"] = uuId;
					const roomData = await allRoomData()
					const joiningRoomData = JSON.parse(roomData).filter((item:any) => item.uuid === uuId)
					// change from find to filter
					const host = await this.roomService.changeHost(uuId);
					// console.log(host)
					res.sendFile(path.resolve("./private/playroom.html"));
					io.emit("room-information",roomData);
					setTimeout(() => {
						io.to(`${uuId}`).emit("new-player",joiningRoomData)
						io.to(host).emit("you-may-not-start");
					},1000)
				}else{
					// res.redirect("/lobby.html")
				}
			}else{
				res.json({message:"This room does not exist"});
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	quitRoom = async(req:Request,res:Response) => {
		try{
			const username = req.session["user"].name;
			const uuId = req.session["uuId"];
			const userId = await this.roomService.getUserId(username)
			const isInRoom = (await this.roomService.userResult(userId)).is_in_room;
			const doesRoomExist = await this.roomService.doesRoomExist(uuId)
			if (doesRoomExist){
				const roomIdResult = await this.roomService.roomIdResult(uuId)
				if (roomIdResult.is_started && (roomIdResult.num_of_players > 1)){
					io.to(uuId).emit("player-quit", username);
					const playerOrder = await this.roomService.getPlayerOrder(username)
					const whichPlayer = await this.roomService.whichPlayer(uuId)
					await this.roomService.updateQuitPlayer(playerOrder,uuId);
					if (whichPlayer === playerOrder){
						const currentTurn = await this.roomService.getCurrentTurn(uuId);
						const quitPlayerArray = await this.roomService.getNumberOfQuitPlayer(uuId);
						const numOfQuitPlayers = quitPlayerArray.length;
						const originalNumOfPlayers = roomIdResult.num_of_players + numOfQuitPlayers - 1;
						endTurn(uuId,currentTurn,originalNumOfPlayers,quitPlayerArray);
					}
					await this.roomService.updateNumberOfPlayers(uuId)
				}
				if (isInRoom){
					await this.roomService.deleteDuplicateUser(userId);
				}
				const roomData = await allRoomData();
				const joiningRoomData = JSON.parse(roomData).filter((item:any) => item.uuid === uuId) //change from find to filter
				io.to(uuId).emit("quit-room", joiningRoomData);
			}
			req.session["uuId"] = 0;
			await playerReset(username);
			res.redirect("/lobby.html");
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}
}