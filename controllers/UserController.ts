import {Request,Response} from "express";
import {checkPassword,hashPassword} from "../hash";
import {User} from "../services/models";
import {UserService} from "../services/UserService";
import crypto from "crypto";
import nodemailer from "nodemailer";
import {logger} from "../logger";
import fetch from 'node-fetch';

export class UserController{
	constructor(private userService:UserService){}

	login = async(req:Request,res:Response) => {
		try{
			const {username, password} = req.body;

			const loginInfos:User[] = await this.userService.loginInfos();
			const userFound = loginInfos.find((user) =>
				// 成功就是user, 失敗就是undefined
				user.name === username
			)

			if(userFound){
				// console.log(userFound.id)
				const match = await checkPassword(password,userFound.password);
				if(match){
					req.session["user"] = {};
					req.session["user"].id = userFound.id;
					req.session["user"].name = userFound.name;
					req.session["user"].email = userFound.email;
					req.session["user"].icon = userFound.icon;
					req.session["loggedIn"] = true;
					res.json({success: true, name: userFound.name});
				}else{
					req.session["user"] = null;
					res.status(302).json({success: false});
				}
			}else{
				req.session["user"] = null;
				res.status(302).json({success: false});
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	googleLogin = async(req:Request,res:Response) => {
		try{
			const accessToken = req.session["grant"].response.access_token;

			const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo',{
				headers: {
					Authorization: `Bearer ${accessToken}`,
				},
			});
			const result = await fetchRes.json();

			let loginInfos = await this.userService.loginInfosByEmail(result.email);
			let user = loginInfos[0];
			if (!user) {
				user = {
					name:result.name,
					email:result.email,
					icon:result.picture,
				};
				const password = await hashPassword(crypto.randomBytes(30).toString());
				await this.userService.createSocialUsers(user.name,password,user.email,user.icon);
			}
			loginInfos = await this.userService.loginInfosByEmail(result.email);
			user = loginInfos[0];
			req.session["user"] = {};
			req.session["user"].id = user.id;
			req.session["user"].name = user.name;
			req.session["user"].email = user.email;
			req.session["user"].icon = user.icon;
			req.session["loggedIn"] = true;
			res.redirect("/lobby.html");
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	facebookLogin = async(req:Request,res:Response) => {
		try{
			const accessToken = req.session["grant"].response.access_token;

			const fetchRes = await fetch(`https://graph.facebook.com/me?access_token=${accessToken}`,{
				headers:{
					Authorization: `Bearer ${accessToken}`,
				},
			})
			const result = await fetchRes.json();

			let loginInfos = await this.userService.loginInfosByName(result.name)
			let user = loginInfos[0];
			if (!user){
				user = {
					name:result.name,
				};
				const password = await hashPassword(crypto.randomBytes(30).toString());
				await this.userService.createSocialUsersWithoutIcon(user.name,password)
			}
			loginInfos = await this.userService.loginInfosByName(result.name)
			user = loginInfos[0];
			req.session["user"] = {};
			req.session["user"].id = user.id;
			req.session["user"].name = user.name;
			req.session["loggedIn"] = true;
			res.redirect("/lobby.html");
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	githubLogin = async(req:Request,res:Response) => {
		try{
			const accessToken = req.session["grant"].response.access_token;
			const fetchRes = await fetch('https://api.github.com/user',{
				headers:{
					Authorization: `Bearer ${accessToken}`,
				},
			})
			const result = await fetchRes.json();

			let loginInfos = await this.userService.loginInfosByName(result.login)
			let user = loginInfos[0];
			if (!user) {
				user = {
					name: result.login,
					icon: result.avatar_url,
				};
				const password = await hashPassword(crypto.randomBytes(30).toString());
				await this.userService.createSocialUsersWithoutEmail(user.name,password,user.icon)
			}
			loginInfos = await this.userService.loginInfosByName(result.login)
			user = loginInfos[0];
			req.session["user"] = {};
			req.session["user"].id = user.id;
			req.session["user"].name = user.name;
			req.session["user"].icon = user.icon;
			req.session["loggedIn"] = true;
			res.redirect('/lobby.html');
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	allUsers = async(req:Request,res:Response) => {
		try{
			const loginInfos = await this.userService.loginInfos();
			res.json(loginInfos);
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	register = async(req:Request,res:Response) => {
		try{
			const registerInfo = req.body;
			if(req.file){
				registerInfo.icon = `/uploads/${req.file.filename}`;
			}
			const hashedPassword = await hashPassword(registerInfo.password);
			await this.userService.register(registerInfo.username,hashedPassword,registerInfo.email,registerInfo.icon,
			registerInfo.birthday,registerInfo.degree)
			res.json({success:true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	checkCurrentUser = async(req:Request,res:Response) => {
		try{
			const userFound = req.session["user"];
			if(userFound){
				res.json({success:true,user:userFound})
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	currentUserInfo = async(req:Request,res:Response) => {
		try{
			const userFound = req.session["user"];
			const loginInfos = (await this.userService.loginInfosByName(req.session["user"].name))[0]
			if(userFound){
				res.json({success:true,user:loginInfos})
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	guest = async(req:Request,res:Response) => {
		try{
			const loginInfos:User[] = await this.userService.orderLoginInfos()
			let number = 0;
			for(const loginInfo of loginInfos) {
				if(loginInfo.name.includes('Guest')){
					number = parseInt(loginInfo.name.substring(6));
				}
			}

			const username = `Guest ${number + 1}`;
			const password = '';
			await this.userService.createGuest(username,password)
			const user = (await this.userService.loginInfosByName(username))[0];
			req.session["user"] = {};
			req.session["user"].id = user.id;
			req.session["user"].name = username;
			req.session["user"].email = '';
			req.session["user"].icon = '';
			res.json({username});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	forgetPasswordEmail = async(req:Request,res:Response) => {
		try{
			const transporter = nodemailer.createTransport({
				host:"smtp.gmail.com",
				port:587,
				secure:false,
				auth:{
					user:process.env.GMAIL_USER,
					pass:process.env.GMAIL_PASS,
				},
			})
			// let success = false;
			const loginInfos:User[] = await this.userService.orderLoginInfos()
			// let info;
			for(const loginInfo of loginInfos){
				if(req.body.email === loginInfo.email){
					const name = loginInfo.name;
					const correctName = name.replace(" ","+");
					await transporter.sendMail({
						from:'emojichattecky@gmail.com', // sender address
						to:req.body.email, // list of receivers
						subject:'Reset your password ✔', // Subject line
						text:`Dear ${name},\nClick the following link to reset your password\n${process.env.WEBSITE_LINK}/resetpassword.html?name=${correctName}`,
					})
					// success = true;
					res.status(200).json({success:true})
					return;
				}
			}
			// const {success} = await forgetPassword(req, res);
			res.json({success:false});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	user = async(req:Request,res:Response) => {
		try{
			const username = req.body.name;

			const usersData = (await this.userService.loginInfosByName(username))[0];
			if(usersData){
				res.json({userData:usersData});
			}else{
				res.json({message:"user not exist"});
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	logout = async(req:Request,res:Response) => {
		try{
			// 只要剷咗session內的user, guard就會阻止你去做edit/delete
			if(req.session){
				delete req.session["user"];
				delete req.session["loggedIn"];
			}
			res.redirect('/');
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	checkPassword = async(req:Request,res:Response) => {
		try{
			const password = await this.userService.checkPassword(req.session["user"].name)
			const check = await checkPassword(req.body.content,password);
			res.json({check});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	updateProfile = async(req:Request,res:Response) => {
		try{
			const registerInfo = req.body;
			// console.log(registerInfo)
			// console.log(req.session["user"].id)
			if(req.file){
				registerInfo.icon = `/uploads/${req.file.filename}`;
			}
			console.log(req.session["user"].id)
			await this.userService.updateUsername(registerInfo.username,req.session["user"].id)
			if(registerInfo.password){
				const hashedPassword = await hashPassword(registerInfo.password);
				await this.userService.updatePassword(hashedPassword,req.session["user"].id)
			}
			await this.userService.updateUserInfo(registerInfo.email,registerInfo.icon,registerInfo.birthday,
			registerInfo.degree,registerInfo.skype,registerInfo.bio,req.session["user"].id)
			req.session["user"].name = registerInfo.username;
			req.session["user"].email = registerInfo.email;
			req.session["user"].icon = registerInfo.icon;
			res.json({success:true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	resetPassword = async(req:Request,res:Response) => {
		try{
			const user = req.body;
			const userFound = await this.userService.loginInfosByName(user.name)
			if(userFound){
				const password = await hashPassword(user.password);
				await this.userService.resetPassword(password,user.name)
			}
			res.json({success: true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}
}