console.log()
const time =new Date()
console.log(time.getFullYear())
console.log(time.getMonth())
console.log(time.getDay())
console.log(time.getHours())
console.log(time.getMinutes())
console.log(time.getSeconds())
//'2013-10-04 22:23:00'

function timeFormat(time){
    return `${time.getFullYear()}-${time.getMonth()}-${time.getDay()} ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`
}

console.log(timeFormat(time))